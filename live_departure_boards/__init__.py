import datetime, re, socket, sys, threading
from collections import namedtuple
from urllib import urlopen
from BeautifulSoup import BeautifulStoneSoup
from BeautifulSoup import BeautifulSoup as HtmlParser

def open_url(url):
    while True:
        try:
            file = urlopen(url)
            return file.read()
        except socket.error:
            pass

def parse_page(page_contents, parser):
    page = HtmlParser(page_contents, convertEntities=BeautifulStoneSoup.XML_ENTITIES)
    return parser(page)

def parse_url(page_url, parser):
    page_contents = open_url(page_url)
    return parse_page(page_contents, parser)

class ExceptionMemento(object):
    def __init__(self, exception=None):
        self.exception = exception or sys.exc_info()[1]
        self.traceback = sys.exc_info()[2]

    def reraise(self):
        raise self.exception, None, self.traceback

class PageParserThread(threading.Thread):
    def __init__(self, page_url, parser):
        super(PageParserThread, self).__init__()
        self.page_url = page_url
        self.parser = parser

    def run(self):
        page_contents = open_url(self.page_url)
        try:
            self._parsed = parse_page(page_contents, self.parser)
        except Exception:
            self.error = ExceptionMemento()

    def get_parsed(self):
        try:
            return self._parsed
        except AttributeError:
            self.error.reraise()


LIVE_DEPARTURES_DOMAIN = 'http://ojp.nationalrail.co.uk'
DEFAULT_STATION_CODES_FILENAME = 'station_codes.csv'
DEFAULT_STATION_CODES_PATH = os.path.join(os.path.basename(__file__), DEFAULT_STATION_CODES_FILENAME)

Station     = namedtuple('Station',     ['name', 'code'])
StationStop = namedtuple('StationStop', ['time', 'station', 'status', 'delay'])

def load_station_codes(filename):
    import csv
    with open(filename, 'rb') as f:
        lines = csv.reader(f)
        lines = list(lines)
    return dict(lines[1:])

def update_station_codes(filename):
    try:
        database = open_url('http://www.nationalrail.co.uk/static/documents/content/station_codes.csv')
    except IOError:
        raise RuntimeError('Error updating station codes')
    else:
        with open(filename, 'w') as f:
            f.write(database)

def get_station_names(filename):
    station_id = load_station_codes(filename)
    return station_id.keys()

def get_stations(filename, *station_names):
    station_ids = load_station_codes(filename)
    try:
        return [Station(station_name, station_ids[station_name]) for station_name in station_names]
    except KeyError as e:
        raise ValueError('Uknown station name "%s"' % e)



class TrainJourneyProxy(object):
    def __init__(self, url):
        self.loading_thread = PageParserThread(LIVE_DEPARTURES_DOMAIN + url, parse_train_schedule)
        self.loading_thread.start()

    def get_station_stops(self):
        try:
            return self._station_stops
        except AttributeError:
            self.loading_thread.join()
            self._station_stops = self.loading_thread.get_parsed()
        return self._station_stops

class MatchesStation(object):
    def __init__(self, station_name):
        self.station_name = station_name

    def __call__(self, station_stop):
        return station_stop.station == self.station_name

class SubJourney(object):
    def __init__(self, journey, departing_from, heading_to):
        self.journey = journey
        self.departing_from = departing_from
        self.heading_to = heading_to

    def get_station_stops(self):
        return slice_between(self.journey.get_station_stops(), MatchesStation(self.departing_from), MatchesStation(self.heading_to))

def get_duration(journey):
    station_stops = journey.get_station_stops()
    return station_stops[-1].time - station_stops[0].time





def islice_between(seq, start, stop):
    started = False
    for item in seq:
        if started or start(item):
            yield item
            started = True
        if started and stop(item):
            break

from operator import eq

def slice_between(seq, start, stop):
    """
    >>> class Equals(object):
    ...     def __init__(self, value):
    ...         self.value = value
    ...     def __call__(self, other):
    ...         return self.value == other
    >>> test_data = [1, 2, 3, 4, 5]
    >>> slice_between(test_data, lambda x: False, lambda x: False)
    []
    >>> slice_between(test_data, lambda x: True, lambda x: False)
    [1, 2, 3, 4, 5]
    >>> slice_between(test_data, lambda x: True, lambda x: True)
    [1]
    >>> slice_between(test_data, Equals(3), Equals(4))
    [3, 4]
    >>> slice_between(test_data, Equals(3), Equals(3))
    [3]
    >>> slice_between(test_data, Equals(3), Equals(2))
    [3, 4, 5]
    """
    return list(islice_between(seq, start, stop))




def parse_live_train_times_page(page):
    results = []

    live_departure_board_div = page.find('div', id='live-departure-board')
    if live_departure_board_div.find('div', {'class': 'results no-results'}):
        return []
    contents_div = live_departure_board_div.find('div', {'class': 'tbl-cont'})
    contents_table = contents_div.find('table')

    for table_row in contents_table.tbody.findAll('tr'):
        cells = table_row.findAll('td')
        results.append(TrainJourneyProxy(cells[4].find('a').get('href')))

    return results

def parse_time(time_str):
    time_pattern = re.compile('(?P<hour>\d+):(?P<minute>\d+)')
    match = time_pattern.match(time_str)
    time = match.groupdict()

    now = datetime.datetime.now()
    return now.replace(hour=int(time['hour']), minute=int(time['minute']))

def parse_station_train_status(status):
    spans = status.findAll('span')
    spans = spans[1].text, spans[0].text
    return spans if spans[0] else (spans[1], 'Scheduled')

def parse_train_schedule(page):
    results = []

    live_departure_board_div = page.find('div', id='live-departure-details')
    contents_div = live_departure_board_div.find('div', {'class': 'tbl-cont'})
    contents_table = contents_div.find('table')

    for table_row in contents_table.tbody.findAll('tr'):
        cells = table_row.findAll('td')
        delay, status = parse_station_train_status(cells[2])
        train = StationStop(
            parse_time(cells[0].text),
            cells[1].text.strip(),
            status,
            delay
        )
        results.append(train)

    return results

def search_trains(departing_from, heading_to):
    page_url = LIVE_DEPARTURES_DOMAIN + '/service/ldbboard/dep/%s/%s/To' % (departing_from.code, heading_to.code)
    train_journeys = parse_url(page_url, parse_live_train_times_page)
    journeys = [SubJourney(train_journey, departing_from.name, heading_to.name) for train_journey in train_journeys]
    return journeys


if __name__ == '__main__':
    station_ids = get_stations(DEFAULT_STATION_CODES_PATH, 'Huddersfield', 'Manchester Piccadilly')
    journeys = search_trains(*station_ids)
    for jouney in journeys:
        print get_duration(jouney)
        for station_stop in jouney.get_station_stops():
            print '%-30s%s' % (station_stop.station, station_stop.time)
        print


