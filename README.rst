
This project was meant to circumvent the licence fees unfairly placed upon travellers whos only crime is to view their
travel details. The project was designed to scrape data from the British National Rail Live Departure Boards website
http://www.nationalrail.co.uk and present this via a friendly API.

This data has since been made public ( See http://data.atoc.org ) and such made this project redundant.



Hello World!
------------

Simple use case::

    >>> import live_departure_boards as ldb
    >>> station_ids = ldb.get_stations(ldb.DEFAULT_STATION_CODES_PATH, 'Huddersfield', 'Manchester Piccadilly')
    >>> journeys = ldb.search_trains(*station_ids)
    >>> for jouney in journeys:
    >>>     print ldb.get_duration(jouney)
    >>>     for station_stop in jouney.get_station_stops():
    >>>         print '%-30s%s' % (station_stop.station, station_stop.time)
    >>>     print

