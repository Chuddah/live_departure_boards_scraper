#!/usr/bin/env python

from ez_setup import use_setuptools
use_setuptools()
from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

setup(
    author='Damien Ruscoe',
    author_email='chuddah@gmail.com',
    url='http://chuddah.ddns.net',

    name='Live Departure Boards',
    description='Access the live departure boards of British National Rail',
    long_description=readme(),
    keywords='trains live planning national rail',
    version='0.1',

    packages=find_packages(),
    include_package_data=True,
    scripts=[
      'bin/nrldb',
    ],
    install_requires=[
      'BeautifulSoup',
    ],
)
